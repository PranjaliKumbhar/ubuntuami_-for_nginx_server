# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}


#Terraform Block
terraform {
  required_version = "v0.14.10"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.36.0"
    }
  }
}

#Simple data module for getting current external IP address
module "myip" {
  source  = "4ops/myip/http"
  version = "1.0.0"
}


#EC2 Instance
resource "aws_instance" "EC2instance" {
  ami             = data.aws_ami.packer_image.id
  instance_type   = "t2.micro"
  key_name        = "packer-key"
  vpc_security_group_ids = [aws_security_group.SSHRule.id]
  tags = {
    Name = "Packer"
  }
}

#KeyPair
resource "aws_key_pair" "owner" {
  key_name   = "packer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQoEPjrxkFvOzT0FJ4WN/m5N1yHyB5RauKxOlJtainkC+SAfw3W4Mtu1hV2c62VxP69ZoRSLJFpH22T1QeUTv9tfR9A5a4qRQWwZZ8+Kcqgn9sahJiTkqpVzNqaPWzoRtvA0fcZi8bfJvggro8r7UsKvAETgUlRVYvdEsbbuR6/g5otsUGelx4eoIv327xKpdix0yY8Xd+Tyim2FiAiEgl5Is1RXqCGAayPSgl10nm3szkZhTm+FcRWtyq7b2+SraJxf4KzvZ9Eljj5Fs+D2YdCOrnKs0j30X5We5C1Mqbp2AI7z8JK59Fn0IyqkFdBvakDooMQnFKgPBR3STJwv/qzbgyzKesBuMGtE+0w2iu2PR48JS+/v9x/VrIC012oD8JF6ROeRoDwd1hB4oYYQDPY3qR3MLrH+GjVfl7LGpaQq/1wghR6vK4eiH4ZXFj4QUabM4HJJQiW0l76MFNc4OdSU/jaHknaGF2lTZmr+PusqrE+FlWCcdPwfQZJuPPzr8= Prajwal@DESKTOP-J088C6I"
}

#Security Group
resource "aws_security_group" "SSHRule" {
  name = "EC2_SSH"

  ingress {
    description = "Allow SSH inbound connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = formatlist("%s/32",[module.myip.address])
  }
  ingress {
    description = "Allow HTTP inbound connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow HTTPS inbound connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    
  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "packer_image" {

    most_recent = true

  filter {
    name   = "name"
    values = ["ami-packer"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["349483170201"] # Canonical
  
}



#Elastic IP
/*
resource "aws_eip" "EIP" {
  instance = aws_instance.EC2instance.id

}
*/
